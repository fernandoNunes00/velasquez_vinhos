package com.velasquezvinho.customer

import com.fasterxml.jackson.databind.ObjectMapper
import com.velasquezvinho.Buy.BuyAPI
import com.velasquezvinho.Customer.CustomerPurchaseService
import com.velasquezvinho.Customer.CustomerController
import com.velasquezvinho.Customer.CustomerService
import com.velasquezvinho.Customer.CustomersAPI
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.client.RestTemplate
import spock.lang.Specification

//TODO - COMO FAZER TESTE INTEGRAÇÃO COM SPRING PARA CONTROLLER (MOCK MVC) (WIRE MOCK)
class CustomerControllerIntegrationTest extends Specification {

    private RestTemplate restTemplate = new RestTemplate()
    private ObjectMapper objectMapper = new ObjectMapper()
    private CustomerPurchaseService customerBiggestPurchaseService = new CustomerPurchaseService(buyAPI)
    private BuyAPI buyAPI = new BuyAPI(restTemplate, objectMapper)
    private CustomersAPI customerAPI = new CustomersAPI(restTemplate, objectMapper)
    private CustomerService customerService = new CustomerService(customerAPI, customerBiggestPurchaseService)
    private CustomerController customerController = new CustomerController(customerService)

    def "Get customer biggest buy should return 200 when the informed year has at least one purchase"() {
        given:

        when:
        ResponseEntity response = customerController.getCustomerBiggestPurchaseByYear(2016)

        then:
        response.getStatusCode() == HttpStatus.OK
    }

    def "Get customer biggest buy should return 404 when the informed year has no purchase"() {
        given:
        customerAPI.urlAPI = "http://www.mocky.io/v2/598b16291100004705515ec5"
        buyAPI.urlAPI = "http://www.mocky.io/v2/598b16861100004905515ec7"

        when:
        ResponseEntity response = customerController.getCustomerBiggestPurchaseByYear(2018)

        then:
        response.getStatusCode() == HttpStatus.NOT_FOUND
    }

    def "Get customer biggest buy should return 400 when informed year has invalid"() {
        given:
        customerAPI.urlAPI = "http://www.mocky.io/v2/598b16291100004705515ec5"
        buyAPI.urlAPI = "http://www.mocky.io/v2/598b16861100004905515ec7"
        Integer year = Calendar.getInstance().get(Calendar.YEAR) + 1

        when:
        ResponseEntity response = customerController.getCustomerBiggestPurchaseByYear(year)

        then:
        response.getStatusCode() == HttpStatus.BAD_REQUEST
    }

    def "Get customers should return 200 when input offset, limit and loyal are valid"() {
        given:
        customerAPI.urlAPI = "http://www.mocky.io/v2/598b16291100004705515ec5"
        buyAPI.urlAPI = "http://www.mocky.io/v2/598b16861100004905515ec7"

        when:
        ResponseEntity response = customerController.getCustomers(false, 0, 20)

        then:
        response.getStatusCode() == HttpStatus.OK
    }
}
