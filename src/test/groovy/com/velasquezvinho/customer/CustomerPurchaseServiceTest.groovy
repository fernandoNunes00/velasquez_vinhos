package com.velasquezvinho.customer

import com.velasquezvinho.Buy.BuyAPI
import com.velasquezvinho.Buy.BuyOutput
import com.velasquezvinho.Buy.Itens
import com.velasquezvinho.Customer.Customer
import com.velasquezvinho.Customer.CustomerPurchaseService
import com.velasquezvinho.Exceptions.NotFoundBuyInThatYear
import com.velasquezvinho.Exceptions.NotFoundCustomerByIdentityRegister
import spock.lang.Specification

//TODO - CRIAR TESTES PARA GET CUSTOMER LOYAL AND GET CUSTOMER ORDER
class CustomerPurchaseServiceTest extends Specification {

    private BuyAPI buyAPI = Mock(BuyAPI)
    private CustomerPurchaseService customerPurchaseService = new CustomerPurchaseService(buyAPI)

    def "Get customer biggest purchase should return a customer when year and customerIdentity is valid"() {
        given:
        List<Customer> customers = []
        List<BuyOutput> sales = []
        List<Itens> items = []
        sales.add(new BuyOutput("123", "2016", "000.000.000.01", items, 200))
        Integer year = 2016
        customers.add(new Customer(01, "Fernando", "000.000.000.01"))
        buyAPI.getAllSales() >> sales

        when:
        Customer response = this.customerPurchaseService.getCustomerBiggestPurchaseInInformedYear(customers, year)

        then:
        noExceptionThrown()
        response != null
    }

    def "Get customer biggest purchase should throw exception when customerIdentity is not found"() {
        given:
        List<Customer> customers = []
        List<BuyOutput> sales = []
        List<Itens> items = []
        sales.add(new BuyOutput("123", "2016", "000.000.000.01", items, 200))
        Integer year = 2016
        customers.add(new Customer(01, "Fernando", "000.000.000.02"))
        buyAPI.getAllSales() >> sales

        when:
        Customer response = this.customerPurchaseService.getCustomerBiggestPurchaseInInformedYear(customers, year)

        then:
        thrown(NotFoundCustomerByIdentityRegister)
        response == null
    }

    def "Get customer biggest purchase should throw exception when is not found any purchase in the informed year"() {
        given:
        List<Customer> customers = []
        List<BuyOutput> sales = []
        List<Itens> items = []
        sales.add(new BuyOutput("123", "2016", "000.000.000.01", items, 200))
        Integer year = 2017
        customers.add(new Customer(01, "Fernando", "000.000.000.01"))
        buyAPI.getAllSales() >> sales

        when:
        Customer response = this.customerPurchaseService.getCustomerBiggestPurchaseInInformedYear(customers, year)

        then:
        thrown(NotFoundBuyInThatYear)
        response == null
    }


}
