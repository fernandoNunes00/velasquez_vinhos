package com.velasquezvinho.customer


import com.velasquezvinho.Customer.CustomerController
import com.velasquezvinho.Customer.Customer
import com.velasquezvinho.Customer.CustomerService
import com.velasquezvinho.Exceptions.InvalidYearError
import com.velasquezvinho.Exceptions.LimitOverflowError
import com.velasquezvinho.Exceptions.NotFoundBuyInThatYear
import com.velasquezvinho.Exceptions.OffsetOutOfLimitError
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import spock.lang.Specification

class CustomerControllerTest extends Specification {

    private CustomerService customerService = Mock(CustomerService)
    private CustomerController customerController = new CustomerController(customerService)

    def "Get customer big purchase by informed year should return 400 when year is invalid"() {
        given:
        this.customerService.getCustomerBiggestPurchaseByYear(2023) >>
                { throw new InvalidYearError("O ano informado é invalido") }

        when:
        ResponseEntity response = this.customerController.getCustomerBiggestPurchaseByYear(2023)

        then:
        response.getStatusCode() == HttpStatus.BAD_REQUEST
    }

    def "Get customer big purchase by informed year should return 404 when not exist purchases on that year"() {
        given:
        this.customerService.getCustomerBiggestPurchaseByYear(2017) >>
                {
                    throw new NotFoundBuyInThatYear
                            ("Nao foi encontrado nenhum cliente com CPF fornecido na compra")
                }

        when:
        ResponseEntity response = this.customerController.getCustomerBiggestPurchaseByYear(2017)

        then:
        response.getStatusCode() == HttpStatus.NOT_FOUND
    }

    def "Get customer big purchase by informed year should return 200 when year is valid"() {
        when:
        ResponseEntity response = this.customerController.getCustomerBiggestPurchaseByYear(2016)

        then:
        response.getStatusCode() == HttpStatus.OK
    }

    def "Get customers should return 400 when limit is above to default"() {
        given:
        this.customerService.getCustomers(false, 0, 21) >>
                { throw new LimitOverflowError("Limite inserido é maior que 20 (Default)") }

        when:
        ResponseEntity response = this.customerController.getCustomers(false, 0, 21)

        then:
        response.getStatusCode() == HttpStatus.BAD_REQUEST
    }

    def "Get customers should return 400 when offset is out of limit"() {
        given:
        this.customerService.getCustomers(false, 40, 19) >>
                { throw new OffsetOutOfLimitError("Offset inserido está estrapolando valor máximo da lista de clientes") }

        when:
        ResponseEntity response = this.customerController.getCustomers(false, 40, 19)

        then:
        response.getStatusCode() == HttpStatus.BAD_REQUEST
    }

    def "Get customers should return 200 when offset and limit are valid"() {
        when:
        ResponseEntity response = this.customerController.getCustomers(false, 0, 10)

        then:
        response.getStatusCode() == HttpStatus.OK
    }
}
