package com.velasquezvinho.customer


import com.velasquezvinho.Buy.BuyOutput
import com.velasquezvinho.Buy.Itens
import com.velasquezvinho.Customer.CustomerPurchaseService
import com.velasquezvinho.Customer.Customer
import com.velasquezvinho.Customer.CustomerService
import com.velasquezvinho.Customer.CustomersAPI
import com.velasquezvinho.Exceptions.NotFoundLoyalCustomers
import spock.lang.Specification

//TODO - ARRUMAR TESTES
class CustomerServiceTest extends Specification {

    private CustomersAPI customersAPI = Mock(CustomersAPI)
    private CustomerPurchaseService customerPurchaseService = Mock(CustomerPurchaseService)
    private CustomerService customerService = new CustomerService(customersAPI, customerPurchaseService)


    def "Get customers biggest purchase should return a customer with no exception when year have a purchase"() {
        given:
        List<Customer> customers = []
        Integer year = 2016
        Customer customer = new Customer(1,"Teste","000.000.000.01")
        customers.add(customer)
        this.customersAPI.getAllCustomers() >> customers
        this.customerPurchaseService.getCustomerBiggestPurchaseInInformedYear(customers,year) >> customer

        when:
        Customer response = this.customerService.getCustomerBiggestPurchaseByYear(year)

        then:
        noExceptionThrown()
        response != null
    }

    def "Get customers should return a list of customers with no exception when fields are correctly"() {
        given:
        List<BuyOutput> sales = new ArrayList<>()
        List<Customer> customerList = new ArrayList<>()
        customerList.add(new Customer(0, "Test", "000.000.000.01"))
        List<Itens> items = new ArrayList<>()
        sales.add(new BuyOutput("123", "2016", "000.000.000.01", items, 200))
        this.customersAPI.getAllCustomers() >> customerList
        this.buyAPI.getAllSales() >> sales

        when:
        List<Customer> response = this.customerService.getCustomers(false, 0, 10)

        then:
        noExceptionThrown()
    }

    def "Get customers should return a list of loyal customers with no exception when field onlyLoyal is true"() {
        given:
        List<BuyOutput> sales = new ArrayList<>()
        List<Customer> customerList = new ArrayList<>()
        customerList.add(new Customer(0, "Test", "000.000.000.01"))
        List<Itens> items = new ArrayList<>()
        sales.add(new BuyOutput("123", "2016", "000.000.000.01", items, 200))
        sales.add(new BuyOutput("123", "2016", "000.000.000.01", items, 200))
        sales.add(new BuyOutput("123", "2016", "000.000.000.01", items, 200))
        sales.add(new BuyOutput("123", "2016", "000.000.000.01", items, 200))
        this.customersAPI.getAllCustomers() >> customerList
        this.buyAPI.getAllSales() >> sales

        when:
        List<Customer> response = this.customerService.getCustomers(true, 0, 10)

        then:
        noExceptionThrown()
    }

    def "Get customers should throws NotFoundLoyalCustomers when customers aren't purchase five or more times"() {
        given:
        List<BuyOutput> sales = new ArrayList<>()
        List<Customer> customerList = new ArrayList<>()
        customerList.add(new Customer(0, "Test", "000.000.000.01"))
        List<Itens> items = new ArrayList<>()
        sales.add(new BuyOutput("123", "2016", "000.000.000.01", items, 200))
        sales.add(new BuyOutput("123", "2016", "000.000.000.01", items, 200))
        sales.add(new BuyOutput("123", "2016", "000.000.000.01", items, 200))
        sales.add(new BuyOutput("123", "2016", "000.000.000.01", items, 200))
        this.customersAPI.getAllCustomers() >> customerList
        this.buyAPI.getAllSales() >> sales

        when:
        List<Customer> response = this.customerService.getCustomers(true, 0, 10)

        then:
        thrown(NotFoundLoyalCustomers)
    }
}
