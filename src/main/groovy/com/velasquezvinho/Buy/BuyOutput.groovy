package com.velasquezvinho.Buy

import com.fasterxml.jackson.annotation.JsonIgnoreProperties

@JsonIgnoreProperties(ignoreUnknown = true)
class BuyOutput {
    private String codigo
    private String data
    private String cliente
    private List<Itens> itens
    private Double valorTotal

    BuyOutput(String codigo, String data, String cliente, List<Itens> itens, Double valorTotal) {
        this.codigo = codigo
        this.data = data
        this.cliente = cliente
        this.itens = itens
        this.valorTotal = valorTotal
    }

    String getCodigo() {
        return codigo
    }

    void setCodigo(String codigo) {
        this.codigo = codigo
    }

    String getData() {
        return data
    }

    void setData(String data) {
        this.data = data
    }

    String getCliente() {
        return cliente
    }

    void setCliente(String cliente) {
        this.cliente = cliente
    }

    List<Itens> getItens() {
        return itens
    }

    void setItens(List<Itens> itens) {
        this.itens = itens
    }

    Double getValorTotal() {
        return valorTotal
    }

    void setValorTotal(Double valorTotal) {
        this.valorTotal = valorTotal
    }
}
