package com.velasquezvinho.Buy

import com.fasterxml.jackson.databind.ObjectMapper
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Service
import org.springframework.web.client.RestTemplate

@Service
class BuyAPI {

    private RestTemplate restTemplate
    private ObjectMapper objectMapper
    private final Integer CUSTOMER_IDENTITY_SIZE = 14
    private final Integer ONE_CHARACTER_REMOVE = 1


    @Value('${buyAPIUrl}')
    private String urlAPI

    BuyAPI(RestTemplate restTemplate, ObjectMapper objectMapper) {
        this.restTemplate = restTemplate
        this.objectMapper = objectMapper
    }

    List<BuyOutput> getAllSales() {
        ResponseEntity<String> response = restTemplate.getForEntity(this.urlAPI, String)
        List<BuyOutput> sales = objectMapper.readValue(response.body, List<BuyOutput>.class)
        return salesSort(clientIdentityFromSalesStandardize(sales))
    }

    private List<BuyOutput> salesSort(List<BuyOutput> sales) {
        return sales.sort { -it.valorTotal }
    }

    private List<BuyOutput> clientIdentityFromSalesStandardize(List<BuyOutput> sales) {
        sales.each {
            if ( it.cliente.length() > CUSTOMER_IDENTITY_SIZE) {
                it.cliente = it.cliente.substring(ONE_CHARACTER_REMOVE)
            }
        }
        return sales
    }

    void setUrlAPI(String urlAPI) {
        this.urlAPI = urlAPI
    }
}
