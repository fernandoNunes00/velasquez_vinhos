package com.velasquezvinho.Buy

import com.fasterxml.jackson.annotation.JsonIgnoreProperties

@JsonIgnoreProperties(ignoreUnknown = true)
class Itens {
    private String codigo
    private String produto
    private String variedade
    private String pais
    private String categoria
    private String safra
    private Double preco

    Itens(String codigo, String produto, String variedade, String pais, String categoria, String safra, Double preco) {
        this.codigo = codigo
        this.produto = produto
        this.variedade = variedade
        this.pais = pais
        this.categoria = categoria
        this.safra = safra
        this.preco = preco
    }

    String getCodigo() {
        return codigo
    }

    void setCodigo(String codigo) {
        this.codigo = codigo
    }

    String getProduto() {
        return produto
    }

    void setProduto(String produto) {
        this.produto = produto
    }

    String getVariedade() {
        return variedade
    }

    void setVariedade(String variedade) {
        this.variedade = variedade
    }

    String getPais() {
        return pais
    }

    void setPais(String pais) {
        this.pais = pais
    }

    String getCategoria() {
        return categoria
    }

    void setCategoria(String categoria) {
        this.categoria = categoria
    }

    String getSafra() {
        return safra
    }

    void setSafra(String safra) {
        this.safra = safra
    }

    Double getPreco() {
        return preco
    }

    void setPreco(Double preco) {
        this.preco = preco
    }
}
