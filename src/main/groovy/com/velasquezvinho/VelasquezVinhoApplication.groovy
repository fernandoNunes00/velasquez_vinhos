package com.velasquezvinho

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.web.client.RestTemplateBuilder
import org.springframework.context.annotation.Bean
import org.springframework.web.client.RestTemplate

@SpringBootApplication
class VelasquezVinhoApplication {

	static void main(String[] args) {
		SpringApplication.run(VelasquezVinhoApplication, args)
	}

	@Bean
	RestTemplate restTemplate(RestTemplateBuilder restTemplateBuilder){
		return restTemplateBuilder.build()
	}

	//TODO CRIAR CLASSE CONFIG PARA CADA DOMAIN - CUSTOMER, BUY, WINE

}
