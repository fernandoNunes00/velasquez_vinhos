package com.velasquezvinho.Customer

import com.velasquezvinho.Buy.BuyAPI
import com.velasquezvinho.Buy.BuyOutput
import com.velasquezvinho.Exceptions.NotFoundBuyInThatYear
import com.velasquezvinho.Exceptions.NotFoundCustomerByIdentityRegister
import com.velasquezvinho.Exceptions.NotFoundLoyalCustomers
import org.springframework.stereotype.Service

@Service
class CustomerPurchaseService {

    private final Integer ONE_PURCHASE = 1
    private final Integer PURCHASES_NEED_TO_BE_LOYAL_CUSTOMER = 5
    private BuyAPI buyAPI

    CustomerPurchaseService(BuyAPI buyAPI) {
        this.buyAPI = buyAPI
    }

    Customer getCustomerBiggestPurchaseInInformedYear(List<Customer> customers, Integer year) {
        List<BuyOutput> sales = this.buyAPI.getAllSales()
        String customerIdentity = getCustomerIdentity(sales, year)
        for (customer in customers) {
            if (customer.cpf.equalsIgnoreCase(customerIdentity)) {
                return new Customer(customer.id, customer.nome, customer.cpf)
            }
        }
        throw new NotFoundCustomerByIdentityRegister("Nao foi encontrado nenhum cliente com CPF fornecido na compra")
    }

    //TODO REVISAR CONCEITO DE SERVICE E REPOSITORY
    List<Customer> getCustomersOrderByMoreHighPurchase(List<Customer> customers,
                                                       Integer offset,
                                                       Integer limit) {
        List<BuyOutput> sales = this.buyAPI.getAllSales()
        Map<String, Boolean> customersOnTheList = [:]
        List<Customer> customersOrderByPurchase = []
        sales.each { sale ->
            if (customersOnTheList.get(sale.cliente) == null && customersOrderByPurchase.size() < limit) {
                customers.each {
                    if (it.cpf == sale.cliente) {
                        customersOrderByPurchase.add(it)
                        customersOnTheList.put(it.cpf, true)
                    }
                }
            }
        }
        return customersOrderByPurchase.subList(offset, customersOrderByPurchase.size())
    }

    List<Customer> getOnlyLoyalCustomers(List<Customer> customers,
                                         Integer offset,
                                         Integer limit) {
        List<BuyOutput> sales = this.buyAPI.getAllSales()
        Map<String, Integer> customersPurchaseQuantity = [:]
        List<Customer> loyalCustomers = []
        sales.each {
            if (isCustomerPurchaseQuantityNull(customersPurchaseQuantity, it.cliente)) {
                customersPurchaseQuantity.put(it.cliente, ONE_PURCHASE)
            } else {
                Integer purchases = customersPurchaseQuantity.get(it.cliente)
                customersPurchaseQuantity.put(it.cliente, purchases + ONE_PURCHASE)
            }
        }
        customersPurchaseQuantity.each { purchaseQuantity ->
            if (purchaseQuantity.getValue() >= PURCHASES_NEED_TO_BE_LOYAL_CUSTOMER && loyalCustomers.size() < limit) {
                customers.each {
                    if (it.cpf == purchaseQuantity.getKey()) {
                        loyalCustomers.add(it)
                    }
                }
            }
        }
        if (isListCustomersNull(loyalCustomers)) {
            throw new NotFoundLoyalCustomers("Não foram encontrados clientes leais")
        }
        return loyalCustomers.subList(offset, loyalCustomers.size())
    }

    private String getCustomerIdentity(List<BuyOutput> sales, Integer year) {
        for (sale in sales) {
            if (sale.data.contains(year.toString())) {
                return sale.cliente
            }
        }
        throw new NotFoundBuyInThatYear("Não foi encontrada nenhuma compra no ano de ${year}")
    }

    private boolean isListCustomersNull(List<Customer> loyalCustomers) {
        return loyalCustomers.size() == 0
    }

    private boolean isCustomerPurchaseQuantityNull(Map<String, Integer> customersPurchaseQuantity,
                                                   String customerIdentity) {
        return customersPurchaseQuantity.get(customerIdentity) == null
    }
}
