package com.velasquezvinho.Customer

class Customer {
    private Integer id
    private String nome
    private String cpf

    Customer(Integer id, String nome, String cpf) {
        this.id = id
        this.nome = nome
        this.cpf = cpf
    }

    Integer getId() {
        return id
    }

    void setId(Integer id) {
        this.id = id
    }

    String getNome() {
        return nome
    }

    void setNome(String nome) {
        this.nome = nome
    }

    String getCpf() {
        return cpf
    }

    void setCpf(String cpf) {
        this.cpf = cpf
    }
}
