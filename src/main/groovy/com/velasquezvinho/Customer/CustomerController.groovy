package com.velasquezvinho.Customer

import com.velasquezvinho.Errors.ErrorBody
import com.velasquezvinho.Exceptions.InvalidYearError
import com.velasquezvinho.Exceptions.LimitOverflowError
import com.velasquezvinho.Exceptions.NotFoundBuyInThatYear
import com.velasquezvinho.Exceptions.NotFoundCustomerByIdentityRegister

import com.velasquezvinho.Exceptions.NotFoundLoyalCustomers
import com.velasquezvinho.Exceptions.OffsetOutOfLimitError
import groovy.util.logging.Slf4j
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Service
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

@Slf4j
@Service
@RestController
@RequestMapping("/customers")
//TODO - TROCAR TRY CATCH POR EXCEPTION HANDLER (WEBEXCEPTIONHANDLER) - APOS VINHOS
class CustomerController {

    private CustomerService customerService

    CustomerController(CustomerService customerService) {
        this.customerService = customerService
    }

    @RequestMapping(value = "/biggest-single-purchase", method = RequestMethod.GET)
    ResponseEntity getCustomerBiggestPurchaseByYear(@RequestParam("year") Integer year) {
        try {
            Customer response = this.customerService.getCustomerBiggestPurchaseByYear(year)
            return new ResponseEntity<Customer>(response, HttpStatus.OK)
        } catch (InvalidYearError error) {
            ErrorBody response = new ErrorBody(error.getMessage())
            return new ResponseEntity<ErrorBody>(response, HttpStatus.BAD_REQUEST)
        } catch (NotFoundBuyInThatYear | NotFoundCustomerByIdentityRegister error) {
            ErrorBody response = new ErrorBody(error.getMessage())
            return new ResponseEntity<ErrorBody>(response, HttpStatus.NOT_FOUND)
        }
    }

    @RequestMapping(method = RequestMethod.GET)
    ResponseEntity getCustomers(@RequestParam("onlyLoyal") boolean onlyLoyal,
                                @RequestParam("offset") Integer offset,
                                @RequestParam("limit") Integer limit) {
        try {
            List<Customer> response = this.customerService.getCustomers(onlyLoyal, offset, limit)
            return new ResponseEntity<List<Customer>>(response, HttpStatus.OK)
        } catch (LimitOverflowError | OffsetOutOfLimitError error) {
            ErrorBody response = new ErrorBody(error.getMessage())
            return new ResponseEntity<ErrorBody>(response, HttpStatus.BAD_REQUEST)
        } catch (NotFoundLoyalCustomers error) {
            ErrorBody response = new ErrorBody(error.getMessage())
            return new ResponseEntity<ErrorBody>(response, HttpStatus.NOT_FOUND)
        }
    }
}
