package com.velasquezvinho.Customer

import com.fasterxml.jackson.databind.ObjectMapper
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Service
import org.springframework.web.client.RestTemplate
import org.springframework.beans.factory.annotation.Value


@Service
//TODO - ISOLAR SPRING DO CODE, SÓ NA CONTROLLER
class CustomersAPI {

    private RestTemplate restTemplate
    private ObjectMapper objectMapper

    @Value('${customersAPIUrl}')
    private String urlAPI

    CustomersAPI(RestTemplate restTemplate, ObjectMapper objectMapper) {
        this.restTemplate = restTemplate
        this.objectMapper = objectMapper
    }

    List<Customer> getAllCustomers() {
        ResponseEntity<String> response = restTemplate.getForEntity(this.urlAPI, String)
        List<Customer> customers = objectMapper.readValue(response.body, List<Customer>.class)
        return customerIdentityStandardize(customers)
    }

    private List<Customer> customerIdentityStandardize(List<Customer> customers) {
        customers.each {
            it.cpf = it.cpf.replace('-', '.')
        }
        return customers
    }
}