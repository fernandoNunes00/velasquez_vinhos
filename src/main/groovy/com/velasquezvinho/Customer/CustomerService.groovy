package com.velasquezvinho.Customer


import com.velasquezvinho.Exceptions.InvalidYearError
import com.velasquezvinho.Exceptions.LimitOverflowError
import com.velasquezvinho.Exceptions.OffsetOutOfLimitError
import groovy.util.logging.Slf4j
import org.springframework.stereotype.Service

@Slf4j
@Service
class CustomerService {

    private final Integer DEFAULT_RESPONSE_LIMIT = 20
    private CustomersAPI customerAPI
    private CustomerPurchaseService customerPurchaseService

    CustomerService(CustomersAPI customerAPI, CustomerPurchaseService customerPurchaseService) {
        this.customerAPI = customerAPI
        this.customerPurchaseService = customerPurchaseService
    }

    Customer getCustomerBiggestPurchaseByYear(Integer year) {
        checkIfTheYearIsValid(year)
        List<Customer> customers = this.customerAPI.getAllCustomers()
        return this.customerPurchaseService.getCustomerBiggestPurchaseInInformedYear(customers, year)
    }

    List<Customer> getCustomers(Boolean onlyLoyal, Integer offset, Integer limit) {
        checkIfTheLimitIsValid(limit)
        List<Customer> customers = customerAPI.getAllCustomers()
        checkIfTheOffsetIsValid(offset, customers.size())
        if (isOnlyLoyalTrue(onlyLoyal)) {
            return this.customerPurchaseService.getOnlyLoyalCustomers(customers, offset, limit)
        } else {
            return this.customerPurchaseService.getCustomersOrderByMoreHighPurchase(customers, offset, limit)
        }
    }

    private void checkIfTheYearIsValid(year) {
        Integer currentYear = Calendar.getInstance().get(Calendar.YEAR)
        if (year > currentYear) {
            throw new InvalidYearError("Ano '${year}' informado é invalido")
        }
    }

    private void checkIfTheLimitIsValid(Integer limit) {
        if (limit > DEFAULT_RESPONSE_LIMIT) {
            throw new LimitOverflowError("Limite inserido é maior que 20 (Default)")
        }
    }

    private void checkIfTheOffsetIsValid(Integer offset, Integer responseLimitSize) {
        if (offset > responseLimitSize) {
            throw new OffsetOutOfLimitError("Offset inserido está estrapolando valor máximo da lista de clientes")
        }
    }

    private boolean isOnlyLoyalTrue(boolean onlyLoyal) {
        return onlyLoyal
    }


}
