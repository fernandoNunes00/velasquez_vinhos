package com.velasquezvinho.Exceptions

class LimitOverflowError extends IndexOutOfBoundsException{
    private String message

    LimitOverflowError(String message) {
        this.message = message
    }

    String getMessage() {
        return message
    }
}
