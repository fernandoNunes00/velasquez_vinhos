package com.velasquezvinho.Exceptions

class InvalidYearError extends IllegalArgumentException{
    private String message

    InvalidYearError(String message) {
        this.message = message
    }

    String getMessage() {
        return message
    }
}
