package com.velasquezvinho.Exceptions

class NotFoundBuyInThatYear extends NullPointerException {
    private String message

    NotFoundBuyInThatYear(String message) {
        this.message = message
    }

    String getMessage() {
        return message
    }
}
