package com.velasquezvinho.Exceptions

class OffsetOutOfLimitError extends IndexOutOfBoundsException{
    private String message

    OffsetOutOfLimitError(String message) {
        this.message = message
    }

    String getMessage() {
        return message
    }
}
