package com.velasquezvinho.Exceptions

class NotFoundLoyalCustomers extends NullPointerException{
    private String message

    NotFoundLoyalCustomers(String message) {
        this.message = message
    }

    String getMessage() {
        return message
    }
}
