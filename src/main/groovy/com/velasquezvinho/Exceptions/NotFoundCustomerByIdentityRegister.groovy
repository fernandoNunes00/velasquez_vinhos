package com.velasquezvinho.Exceptions

class NotFoundCustomerByIdentityRegister extends NullPointerException{
    private String message

    NotFoundCustomerByIdentityRegister(String message) {
        this.message = message
    }

    String getMessage() {
        return message
    }
}
